Thoughts/Notes on CIM Sinergien project
=======================================

## Project structure
- All repositories should be in one handy group.
- Get rid of the old Generated Code repository.
  - This was my personal working repository. [mmi-dkn]

## CGMES
- Reorganization of the Packages
  - The CGMES standard seems to be reorganized into different profiles compared to CIM standard. This seems to be a problem for the sinergien toolchain.
  - Due to the reorganization into profiles there are multiple different definitions of the same classes containing different members.
  - There seems to be no "Super definition" of these classes.
- Entsoe extension
  - The entsoe extension is part of the CGMES standard.
  - Each entsoe member is marked as "Entsoe" stereotype in the UML model.
  - The entsoe extension uses a different namespace in the XML files.
  - When generating the C++ code there is no such marking of the entsoe extension.

## Tasks for new students
1. Read in Metadata of CIM/CGMES XML files
  - To interpret the data of the XML files it can be necessary to know which tool generated them.
  - Read and provide metadata from XML files.
2. Give warning when parsing information of different XML namespaces
  - It can be useful to inform the user when information of the XML files in discarded due to unknown XML namespaces.
  - Provide a waring when discarding information of any kind!

## Faults in CIM standard
#### In Version cim16v29a
1. In class `ExcBBC` the C/C++ keyword `switch` is used as a member identifier!
   For the CIM sinergien parser there is a workaround for this problem. See [fix004.patch](https://git.rwth-aachen.de/CIM-XML-Interface/Refactoring-Script/blob/master/Patches/fix004.patch).
2. In class `ReportingSuperGroup` exists a nested class `ReportingGroup` and a member `ReportingGroup`. This results in a namespace collision.
   For the CIM sinergien parser there is a workaround for this problem. See [fix008.patch](https://git.rwth-aachen.de/CIM-XML-Interface/Refactoring-Script/blob/cim16v29a/Patches/fix008.patch).

These faults could be send to the standardization committee.

## Things to know when creating new CIM packages
#### Domain like data types
When creating new data types like those in the `Domain` package, keep in mind: these data types are classes!
Therefore when choosing type, select `Class`.

#### Enumerations
Enumerations are like the domain data types classes with the stereotype `enumeration`. The enumeration symbols have the stereotype `enum`.
Important: First select type `Class` then change the stereotype to `enumeration` with a lowercase letter!
The type will automatically change to `Enumeration`. Keep it like that!

#### Member data types
All member data types have to be from the Common Information Model or it's extensions! E.g. use `Base::Domain::Float` instead of `float`.

## Possible reimplementation of the CIM parser
- When generating the CIM C++ code for the parser lots of information is lost. An alternative approach would be to parse the XMI files to gain every information needed for unmarshalling the CIM XML files.

## Problems with Enterprise Architect
#### C++ scoped enumerations
Enterprise Architect is not capable of generating scoped enumerations like the following example:
``` c++
enum class Color
{
  Red, Green, Blue
}

Color color = Color::Red;
```
The CIM standard uses the same enumeration symbol in more than one enumeration. The problem with this is, that enumeration symbols must be unique in C/C++, otherwise it will cause a redefinition error. The only reasonable solution to this is the usage of C++'s scoped enumeration. Since Enterprise Architect can not generate code for this. The [CIMRefactorer](CIM-XML-Interface/CIMRefactorer) changes each enumeration to a scoped enumeration.
Please refer to [stackoverflow](http://stackoverflow.com/questions/34065152/scoped-enumeration-using-enterprise-architect-c-forward-generation).

#### Generation of incomplete include directives
When generating CIM C++ from the Enterprise Architect UML model, it will generate each file in a directory corresponding to is CIM package hierarchy. A class in package `Core` will have a file generated in a subdirectory called `Core` and so on. Therefore include directives need to have a relative path to the root directory. Enterprise Architect however does only generate `#include "filename"`.
###### Possible solutions
1. Put all files in one directory
2. Modify include directives to contain relative paths

The second solution is the solution used by the CIM parser. The [CIM-Include-Tool](CIM-XML-Interface/CIM-Include-Tool) modifies existing include directives.

#### Enterprise Architect does not know C++11's smart pointers
Note: Smart pointers were not used for the CIM parser.

#### No generation of include directives for used container classes possible
Enterprise Architect code generator allows the usage of custom container classes like `std::list`. But there is no possibility to configure include directives for the container classes. The [CIM-Include-Tool](CIM-XML-Interface/CIM-Include-Tool) adds an `#include <list>` in every file which contains a `std::list`.

#### Enterprise Architect does not check of a forward declaration is useful or necessary

#### Enterprise Architect does not generate POSIX conform text files
The POSIX standard defines that every text file has to end with a new line symbol. Enterprise Architect only generates a new line symbol for header files not for *.cpp files. According to [Sparx Systems](http://sparxsystems.com/forums/smf/index.php?topic=532.0) this should already be fixed.
